import React, { Component } from 'react';

import { Button, Card, message, Popconfirm } from 'antd';

import isEmpty from 'lodash/isEmpty';

import { IMAGE_HOST, URL_ENDPOINTS } from 'common/constants';

import requestUtil from 'util/requestUtil';

import WORDS from 'common/words';

const { Meta } = Card;

class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      defaultParams: null,
      products: []
    };
  }

  componentDidMount() {
    this.getProducts();
  }

  getProducts = () => {
    requestUtil
      .get(URL_ENDPOINTS.CAKES_GET, { type: this.props.productType })
      .then(({ products, success }) => {
        if (success) {
          this.setState({
            defaultParams: null,
            products
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <div className="products-container">
        {this.state.products.map((product) => (
          <div className="product" key={product._id}>
            <Card
              className="product-card"
              cover={<img alt="example" src={`${IMAGE_HOST}${product.photo}`} />}
              hoverable
              title={product.title}
            >
              <Meta description={product.description} />
            </Card>
          </div>
        ))}
      </div>
    );
  }
}

export default Products;
