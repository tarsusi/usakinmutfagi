import React from 'react';
import { withRouter, Link } from 'react-router-dom';

import { Menu, Icon } from 'antd';

const menuList = [
  { title: 'Kekler', icon: '', path: '/kekler' },
  { title: 'Kurabiyeler', icon: '', path: '/kurabiyeler' },
  { title: 'Poğaçalar', icon: '', path: '/pogacalar' },
  { title: 'Pastalar', icon: '', path: '/pastalar' },
  { title: 'Tatlılar', icon: '', path: '/tatlilar' },
  { title: 'Sütlü Tatlılar', icon: '', path: '/sutlu-tatlilar' },
  { title: 'Salatalar', icon: '', path: '/salatalar' },
  { title: 'Yemekler', icon: '', path: '/yemekler' },
  { title: 'Organik ürünler', icon: '', path: '/organik-urunler' },
  { title: 'Blog', icon: '', path: '/blog' },
  { title: 'Videolar', icon: '', path: '/videolar' },
  { title: 'İletişim', icon: '', path: '/iletisim' }
];

const LeftMenu = (props) => (
  <Menu className="left-menu-container" defaultSelectedKeys={[props.location.pathname]}>
    {menuList.map((menu) => (
      <Menu.Item key={menu.path} onClick={props.onMenuItemClick}>
        <Link to={menu.path}>{menu.title}</Link>
      </Menu.Item>
    ))}
  </Menu>
);

export default withRouter(LeftMenu);
