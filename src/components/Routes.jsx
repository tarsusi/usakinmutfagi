import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';

import Products from 'components/products/Products';
import MyProfile from 'components/my-profile/MyProfile';
import Contact from 'components/contact/Contact';

const Routes = () => (
  <Switch>
    <Route exact path="/" render={() => <Redirect to="/kekler" />} />
    <Route path="/kekler" component={(props) => <Products {...props} productType="kekler" />} />
    <Route path="/kurabiyeler" component={(props) => <Products {...props} productType="kurabiyeler" />} />
    <Route path="/pogacalar" component={(props) => <Products {...props} productType="pogacalar" />} />
    <Route path="/pastalar" component={(props) => <Products {...props} productType="pastalar" />} />
    <Route path="/tatlilar" component={(props) => <Products {...props} productType="tatlilar" />} />
    <Route path="/sutlu-tatlilar" component={(props) => <Products {...props} productType="sutluTatlilar" />} />
    <Route path="/salatalar" component={(props) => <Products {...props} productType="salatalar" />} />
    <Route path="/yemekler" component={(props) => <Products {...props} productType="yemekler" />} />
    <Route path="/organik-urunler" component={(props) => <Products {...props} productType="organikUrunler" />} />
    <Route path="/blog" component={MyProfile} />
    <Route path="/videolar" component={MyProfile} />
    <Route path="/iletisim" component={Contact} />
  </Switch>
);

export default Routes;
