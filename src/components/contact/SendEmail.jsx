import React, { Component } from 'react';

import { Form, Input, Button } from 'antd';

import { URL_ENDPOINTS } from 'common/constants';

import requestUtil from 'util/requestUtil';

class SendEmail extends Component {
  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.sendEmail(values);
      }
    });
  };

  sendEmail = ({ name, email, message, subject }) => {
    requestUtil
      .post(URL_ENDPOINTS.FEEDBACK, { name, email, message, subject })
      .then(({ result, success }) => {
        if (success) {
          console.log(result);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} className="send-mail-form">
        <div className="form-title">Lütfen düşüncelerinizi bizimle paylaşın.</div>
        <Form.Item label="İsim">{getFieldDecorator('name', {})(<Input placeholder="İsminizi giriniz..." />)}</Form.Item>
        <Form.Item label="Mail Adresi">
          {getFieldDecorator('email', {})(<Input placeholder="usagin@mutfagi.com" />)}
        </Form.Item>
        <Form.Item label="Mail Başlığı">
          {getFieldDecorator('subject', {
            rules: [{ required: true, message: 'Lütfen mail başlığı giriniz.' }]
          })(<Input placeholder="Başlık" />)}
        </Form.Item>
        <Form.Item label="Mesaj">
          {getFieldDecorator('message', {
            rules: [{ required: true, message: 'Lütfen mesajınızı giriniz.' }]
          })(<Input.TextArea placeholder="Mesajınız..." />)}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Gönder
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default Form.create({ name: 'send_email' })(SendEmail);
