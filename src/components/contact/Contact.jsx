import React from 'react';

import SendEmail from './SendEmail';
import { Icon } from 'antd';

const SocialIcons = () => {
  return (
    <div className="social-icons-container">
      <a className="facebook-share" href="https://www.facebook.com/usakin.mutfagi" target="_blank">
        <Icon type="facebook" theme="filled" />
        <span className="address">usakin.mutfagi</span>
      </a>
      <a className="instagram-share" href="https://www.instagram.com/usakinmutfagi/" target="_blank">
        <Icon type="instagram" theme="filled" />
        <span className="address">usakinmutfagi</span>
      </a>
    </div>
  );
};

const Contact = () => (
  <div className="contact-container">
    <SendEmail />
    <SocialIcons />
  </div>
);

export default Contact;
