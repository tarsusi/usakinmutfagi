import React from 'react';

import LeftMenu from 'components/left-menu/LeftMenu';
import MyDrawer from 'components/drawer/Drawer';
import MyHeader from 'components/header/Header';

import Routes from 'components/Routes';
import { Layout } from 'antd';

const { Header, Sider, Content } = Layout;

class App extends React.Component {
  state = {
    isMenuOpen: false
  };

  onClose = () => {
    this.setState({
      isMenuOpen: false
    });
  };

  toggleMenu = () => {
    this.setState((prevState) => ({
      isMenuOpen: !prevState.isMenuOpen
    }));
  };

  render() {
    return (
      <Layout className="app-container">
        <Header>
          <MyHeader toggleMenu={this.toggleMenu} />
        </Header>
        <Layout>
          <Sider width={225} theme="light">
            <LeftMenu />
          </Sider>
          <MyDrawer isMenuOpen={this.state.isMenuOpen} onClose={this.onClose} onClick={this.onClose} />
          <Content className="content-container">
            <Routes />
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default App;
