import React from 'react';

import { Button } from 'antd';

const HeaderComponent = (props) => (
  <div className="header-container">
    <Button
      className="header-toggle-button"
      icon="bars"
      size="large"
      type="default"
      onClick={() => {
        props.toggleMenu();
      }}
    />
    <div className="header-title">Uşak'ın Mutfağı</div>
  </div>
);

export default HeaderComponent;
