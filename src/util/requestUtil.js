import axios from 'axios';
import { URL_HOST } from 'common/constants';

class RequestUtil {
  constructor() {
    this.axios = axios.create({
      baseURL: URL_HOST,
      headers: {
        common: {
          'Access-Control-Allow-Origin': '*',
          Accept: 'application/json, text/plain, */*'
        }
      }
    });
  }

  get = (url, params) => {
    return this.axios
      .get(url, { params })
      .then((response) => response.data)
      .catch((err) => err);
  };

  post = (url, params) => {
    return this.axios
      .post(url, params)
      .then((response) => response.data)
      .catch((err) => err);
  };
}

export default new RequestUtil();
