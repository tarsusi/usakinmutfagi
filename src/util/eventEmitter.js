import { EventEmitter } from 'events';

class EventsEmitter {
  constructor() {
    this.eventEmitter = new EventEmitter();
  }

  on = (event, payload) => {
    this.eventEmitter.on(event, payload);
  };

  emit = (event, payload) => {
    this.eventEmitter.emit(event, payload);
  };
}

export default new EventsEmitter();
